import { Component, OnInit } from '@angular/core';
import { HttpService } from './http.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'star-wars-app';

  categories: string[]

  person
  planet
  vehicle
  species
  starship

  show: boolean = false

  selectedItem: string = "people"

  selectedID: number = 1

  constructor(private http: HttpService) {}

  findStarWarsThing() {

    this.show = true

    this.person = this.planet = this.vehicle = this.species = this.starship = null

    this.http.getStarWarsThing(this.selectedItem, this.selectedID)
      .subscribe((data) => {

        this.show = false

        switch (this.selectedItem) {
          case "people": {
            this.person = data
            break
          }
          case "planets": {
            this.planet = data
            break
          }
          case "vehicles": {
            this.vehicle = data
            break
          }
          case "species": {
            this.species = data
            break
          }
          case "starships": {
            this.starship = data
          }
          default: {
            console.error("DID NOT FIND VIABLE CATEGORY"); 
          } 
        }
    })
  }

  ngOnInit() {

    // get categories and put them into an array
    this.http.getStarWarsCategories()
      .subscribe((categories: Object) => {
        let tempArr = []

        Object.keys(categories).forEach(item => {
          if(item !== "films") {
            tempArr.push(item)
          }
        })
        this.categories = tempArr

      })

  }

}

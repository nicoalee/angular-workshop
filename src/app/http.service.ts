import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { catchError, retry } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  // api source
  starWarsLink: string = "https://swapi.co/api/"

  constructor(private http: HttpClient) { }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message)
      alert(error.error.message)
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      alert(
        `Backend returned code ${error.status}, ` +
        `Error: ${error.error.detail}`
      )
      console.error(
        `Backend returned code ${error.status}, ` +
        `Error: ${error.error.detail}`)
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.')
  }

  getStarWarsThing(category:string, id:number) {

    return this.http.get(`${this.starWarsLink}${category}/${id}`)
    .pipe(catchError(this.handleError))

  }

  getStarWarsCategories() {

    return this.http.get(this.starWarsLink)
  }
}

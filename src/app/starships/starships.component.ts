import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-starships',
  templateUrl: './starships.component.html',
  styleUrls: ['./starships.component.css']
})
export class StarshipsComponent implements OnInit {
  @Input() starship
  constructor() { }

  ngOnInit() {
  }

}

import { People } from './people';
import { Film } from './film';

export class Vehicle {
    name: string
    model: string
    manufacturer: string
    cost_in_credit: string
    length: string
    max_atomospheringSpeed: string
    crew: string
    passengers: string
    cargo_capacity: string
    consumables: string
    vehicle_class: string
    pilots: People[]
    films: Film[]
    created: string
    edited: string
    url: string
}
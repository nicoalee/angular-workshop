import { People } from './people';
import { Planet } from './planet';
import { Film } from './film';

export class Species {
    name: string
    classification: string
    designation: string
    average_height: string
    skin_colors: string
    hair_colors: string
    eye_colors: string
    average_lifespan: string
    homeworld: Planet
    language: string
    people: People[]
    films: Film[]
    created: string
    edited: string
    url: string
}
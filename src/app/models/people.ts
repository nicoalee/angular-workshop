import { Planet } from './planet';
import { Starship } from './starship';
import { Vehicle } from './vehicle';
import { Species } from './species';
import { Film } from './film';

export class People {
    name: string
    height: string
    mass: string
    hair_color: string
    skin_color: string
    eye_color: string
    birthYear: string
    gender: string
    homeworld: string
    films: Film[]
    species: Species[]
    vehicles: Vehicle[]
    starships: Starship[]
    created: string
    edited: string
    url: string
}
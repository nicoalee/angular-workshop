import { People } from './people';
import { Planet } from './planet';
import { Starship } from './starship';
import { Vehicle } from './vehicle';
import { Species } from './species';


export class Film {
    title: string
    episode_id: number
    opening_crawel: string
    director: string
    producer: string
    release_date: string
    characters: People[]
    planets: Planet[]
    starships: Starship[]
    vehicles: Vehicle[]
    species: Species[]
    created: string
    edited: string
    url: string
}
import { People } from './people';
import { Film } from './film';

export class Planet {
    name: string
    rotation_period: string
    orbital_period: string
    diameter: string
    climate: string
    gravity: string
    terrain: string
    surfaceW_water: string
    population: string
    residents: People[]
    films: Film[]
    created: string
    edited: string
    url: string
}
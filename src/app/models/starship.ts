import { People } from './people';
import { Film } from './film';

export class Starship {
    name: string
    model: string
    manufacturer: string
    cost_in_credit: string
    length: string
    max_atomospheringSpeed: string
    crew: string
    passengers: string
    cargo_capacity: People[]
    consumables: string
    hyperdrive_rating: string
    MGLT: string
    starship_class: string
    pilots: People[]
    films: Film[]
    created: string
    edited: string
    url: string
}
import { Component, OnInit, Input } from '@angular/core';
import { HttpService } from '../http.service'
@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.css']
})
export class FilmsComponent implements OnInit {

  constructor(private httpService: HttpService) { }
  @Input() films
  ngOnInit() {
  }

}
